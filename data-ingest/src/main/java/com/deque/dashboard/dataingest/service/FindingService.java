package com.deque.dashboard.dataingest.service;

import com.deque.dashboard.dataingest.repository.HBaseRepository;
import com.deque.dashboard.record.Finding;
import com.google.protobuf.ServiceException;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class FindingService {
    private HBaseRepository repository;

    public FindingService(HBaseRepository repository) {
        this.repository = repository;
    }

    public boolean saveFindings(List<Finding> findings) throws IOException, ServiceException {
        return repository.saveFindings(findings);
    }
}
