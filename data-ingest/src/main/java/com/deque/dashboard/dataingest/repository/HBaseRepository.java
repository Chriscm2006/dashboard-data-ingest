package com.deque.dashboard.dataingest.repository;

import com.deque.dashboard.dataingest.dao.HBaseAvroClient;
import com.deque.dashboard.record.Finding;
import com.google.protobuf.ServiceException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.List;

@Slf4j
@Repository
public class HBaseRepository {

    private HBaseAvroClient hbaseClient;

    public HBaseRepository(HBaseAvroClient client) {
        this.hbaseClient = client;
    }

    /**
     * save all the findings.
     * @param findings list of findings
     * @return true if successful
     */
    public boolean saveFindings(List<Finding> findings) throws IOException, ServiceException {
        return hbaseClient.saveFindings(findings);
    }
}
