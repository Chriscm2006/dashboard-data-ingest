package com.deque.dashboard.dataingest.controller;

import com.deque.dashboard.dataingest.service.FindingService;
import com.deque.dashboard.record.Finding;
import com.google.protobuf.ServiceException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@RestController
public class FindingController {

    private FindingService service;

    public FindingController(FindingService service) {
        this.service = service;
    }

    @RequestMapping("/")
    public Object greeting() {
        return "Deque Dashboard Ingestion Service is running....";
    }

    /**
     * A test method to return a finding.
     * @return Finding
     */
    @RequestMapping(value = "/finding")
    public ResponseEntity<Finding> get() {

        Finding finding = new Finding();
        finding.setId("test_id");
        finding.setIsIgnored(false);
        finding.setLocation("test_location");
        finding.setOrganization("test_org");
        finding.setIsManual(true);

        return new ResponseEntity<>(finding, HttpStatus.OK);
    }

    /**
     * A test method to save one finding.
     * @param finding input finding
     * @return HttpStatus
     */
    @RequestMapping(method = RequestMethod.POST, value = "/finding")
    public ResponseEntity<Finding> saveFinding(@RequestBody Finding finding) {
        ArrayList<Finding> findings = new ArrayList<>();
        findings.add(finding);
        return saveFindings(findings);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/findings")
    public ResponseEntity saveFindings(@RequestBody List<Finding> findings) {

        try {
            service.saveFindings(findings);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(400).body(e.getMessage());
        }
    }
}
