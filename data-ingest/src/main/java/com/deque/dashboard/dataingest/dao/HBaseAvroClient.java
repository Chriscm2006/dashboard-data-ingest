package com.deque.dashboard.dataingest.dao;

import com.deque.dashboard.record.Finding;

import com.fasterxml.jackson.dataformat.avro.ser.NonBSGenericDatumWriter;
import com.google.protobuf.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileStream;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.reflect.ReflectDatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecordBase;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Slf4j
@Component
public class HBaseAvroClient {
    private Configuration conf;

    private final TableName findingTable = TableName.valueOf("finding");

    private final String dashboardCf = "dashboard";

    private final String findingC = "finding";

    private static final int DEFAULT_REGIONS = 4;

    public HBaseAvroClient() {
        this.conf = HBaseConfiguration.create();
    }

    /**
     * Save All Findings.
     * @param findings list of findings
     * @return true if successful
     * @throws IOException Connection Error
     * @throws ServiceException Service Error
     */
    public boolean saveFindings(List<Finding> findings) throws IOException, ServiceException {

        Configuration config = HBaseConfiguration.create();

        HBaseAdmin.checkHBaseAvailable(config);

        Connection connection = ConnectionFactory.createConnection(conf);
        Admin admin = connection.getAdmin();

        if (!admin.tableExists(findingTable)) {
            createTable(admin);
        }

        Table table = connection.getTable(findingTable);

        for (Finding f : findings) {
            put(table, generateRowKey(f), f);
        }

        return true;
    }

    private void createTable(Admin admin) throws IOException {
        HTableDescriptor newTable = new HTableDescriptor(findingTable);

        //final byte[][] splits = new RegionSplitter.HexStringSplit().split(DEFAULT_REGIONS);
        newTable.addFamily(new HColumnDescriptor(dashboardCf));
        admin.createTable(newTable);
    }

    private byte[] generateRowKey(Finding finding) {
        String key = String.format("%s%s%s%d",
                finding.getSource(),
                finding.getLocation(),
                finding.getOrganization(),
                System.currentTimeMillis());

        return DigestUtils.md5(key);
    }

    private void put(Table table, final byte[] rowKey, Finding finding) throws IOException {

        System.out.println(finding.toString());
        final ByteArrayOutputStream stream = new ByteArrayOutputStream();

        final DatumWriter<Finding> datumWriter = new NonBSGenericDatumWriter<>(finding.getSchema());
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(stream, null);
        datumWriter.write(finding, encoder);

        encoder.flush();

        Put put = new Put(rowKey);
        put.addImmutable(dashboardCf.getBytes(), findingC.getBytes(), stream.toByteArray());
        table.put(put);
    }

    /**
     * delete a table.
     * @param name name of the table
     * @throws IOException IO Error
     */
    public void deleteTable(final String name) throws IOException {
        try (
                Connection connection = ConnectionFactory.createConnection(conf);
                Admin admin = connection.getAdmin()
        ) {
            final TableName tname = TableName.valueOf(name);

            if (admin.tableExists(tname)) {
                admin.disableTable(tname);
                admin.deleteTable(tname);
            }
        }
    }

    /**
     * get a finding.
     * @param rowkey id
     * @param schema schema
     * @param <T> return type
     * @return a record
     * @throws IOException IO Error
     */
    public <T extends SpecificRecordBase> T get(
            final byte[] rowkey,
            final Schema schema) throws IOException {

        T got = null;

        final Get get = new Get(rowkey);
        get.addColumn(dashboardCf.getBytes(), findingC.getBytes());

        try (Connection connection = ConnectionFactory.createConnection(conf)) {
            final Table htable = connection.getTable(findingTable);

            final Result res = htable.get(get);

            final ByteArrayInputStream stream = new ByteArrayInputStream(res.value());
            final DatumReader<T> reader = new SpecificDatumReader<>(schema);
            final DataFileStream<T> fileStream = new DataFileStream<>(stream, reader);

            return fileStream.next(got);
        }
    }
}
