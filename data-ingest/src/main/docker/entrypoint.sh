#!/bin/sh

shutdown () {
  kill -s SIGTERM $NODE_PID
  wait $NODE_PID
}

java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar /data-ingest.war &

NODE_PID=$!

trap shutdown SIGTERM SIGINT
wait $NODE_PID